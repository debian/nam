#ifndef _CONFIG_H_
#define _CONFIG_H_
#cmakedefine PACKAGE_BUGREPORT "${PACKAGE_BUGREPORT}"
#cmakedefine PACKAGE_NAME "${PACKAGE_NAME}"
#cmakedefine PACKAGE_STRING "${PACKAGE_STRING}"
#cmakedefine PACKAGE_TARNAME "${PACKAGE_TARNAME}"
#cmakedefine PACKAGE_VERSION "${PACKAGE_VERSION}"
#define RANDOM_RETURN_TYPE long int
#cmakedefine HAVE_BCOPY 1
#cmakedefine HAVE_BZERO 1
#cmakedefine HAVE_GETRUSAGE 1
#cmakedefine HAVE_SBRK 1
#cmakedefine HAVE_SNPRINTF 1
#cmakedefine HAVE_STRTOLL 1
#cmakedefine HAVE_STRTOD 1
#cmakedefine HAVE_STRTOQ 1
#cmakedefine HAVE_STRING_H 1
#cmakedefine HAVE_STRINGS_H 1
#endif //_CONFIG_H_
