project(nam)
cmake_minimum_required(VERSION 2.6)
set(TARNAME nam)


# program name, version etc

set(PACKAGE_VERSION "1.15")
set(PACKAGE_NAME "nam")
set(PACKAGE_TARNAME "${TARNAME}")
set(PACKAGE_STRING "${PACKAGE_NAME} ${PACKAGE_VERSION}")
set(PACKAGE_BUGREPORT "http://sourceforge.net/projects/nsnam")

find_package(PkgConfig REQUIRED)

##########################################################################
if(NOT DEFINED BIN_INSTALL_DIR)
    set(BIN_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/bin")
endif(NOT DEFINED BIN_INSTALL_DIR)
if(NOT DEFINED DATA_INSTALL_DIR)
    set(DATA_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}/share")
endif(NOT DEFINED DATA_INSTALL_DIR)
if(NOT DEFINED MAN_INSTALL_DIR)
    set(MAN_INSTALL_DIR "${DATA_INSTALL_DIR}/man")
endif(NOT DEFINED MAN_INSTALL_DIR)

find_package(TCL REQUIRED)
find_package(TclStub REQUIRED)
pkg_check_modules(OTCL REQUIRED otcl)
pkg_check_modules(TCLCL REQUIRED tclcl)

INCLUDE (CheckFunctionExists)
INCLUDE (CheckIncludeFiles)

CHECK_INCLUDE_FILES(string.h HAVE_STRING_H)
CHECK_INCLUDE_FILES(strings.h STRINGS_H)

CHECK_FUNCTION_EXISTS(bcopy HAVE_BCOPY)
CHECK_FUNCTION_EXISTS(bzero HAVE_BZERO)
CHECK_FUNCTION_EXISTS(getrusage HAVE_GETRUSAGE)
CHECK_FUNCTION_EXISTS(sbrk HAVE_SBRK)
CHECK_FUNCTION_EXISTS(strtoll HAVE_STRTOLL)
CHECK_FUNCTION_EXISTS(strtod HAVE_STRTOD)
CHECK_FUNCTION_EXISTS(strtoq HAVE_STRTOQ)
CHECK_FUNCTION_EXISTS(snprintf HAVE_SNPRINTF)

add_definitions(-DUSE_INTERP_RESULT)

configure_file(config-nam.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/autoconf.h)

set(NAM_TCL_LIB 
	tcl/nam-lib.tcl
	tcl/nam-default.tcl
	tcl/balloon.tcl
	tcl/snapshot.tcl
	tcl/animator.tcl
	tcl/anim-ctrl.tcl
	tcl/netModel.tcl
	tcl/autoNetModel.tcl
	tcl/build-ui.tcl
	tcl/annotation.tcl
	tcl/node.tcl
	tcl/monitor.tcl
	tcl/stats.tcl
	tcl/www.tcl
	tcl/menu_file.tcl
	tcl/menu_view.tcl
	tcl/NamgraphView.tcl
	tcl/NamgraphModel.tcl
	tcl/TimesliderNamgraphView.tcl
	tcl/TimesliderView.tcl
	tcl/TimesliderModel.tcl
	tcl/observer.tcl
	tcl/observable.tcl
	tcl/wirelessNetModel.tcl
	tcl/editorNetModel.tcl
	tcl/Editor.tcl
	tcl/Editor-FileParser.tcl
)
STRING(REPLACE ";" "	" NAM_TCL_LIB "${NAM_TCL_LIB}")

add_custom_command(OUTPUT version.c nam_tcl.cc
	COMMAND ${TCL_TCLSH} ${CMAKE_CURRENT_SOURCE_DIR}/bin/string2c.tcl version < ${CMAKE_CURRENT_SOURCE_DIR}/VERSION > version.c
	COMMAND ${TCL_TCLSH} ${CMAKE_CURRENT_SOURCE_DIR}/bin/tcl-expand.tcl ${CMAKE_CURRENT_SOURCE_DIR}/tcl/nam-lib.tcl | tcl2c++ et_nam > nam_tcl.cc
)

include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${TCL_INCLUDE_PATH}
    ${TK_INCLUDE_PATH}
    ${OTCL_INCLUDE_DIRS}
    ${TCLCL_INCLUDE_DIRS}
)

set(nam_SRC
    version.c
    nam_tcl.cc
    tkcompat.c
    tkUnixInit.c
    xwd.c
    netview.cc
    netmodel.cc
    edge.cc
    packet.cc
    node.cc
    main.cc
    trace.cc
    queue.cc
    drop.cc
    animation.cc
    agent.cc
    feature.cc
    route.cc
    transform.cc
    paint.cc
    state.cc
    monitor.cc
    anetmodel.cc
    rng.cc
    view.cc
    graphview.cc
    netgraph.cc
    tracehook.cc
    lan.cc
    psview.cc
    group.cc
    editview.cc
    tag.cc
    address.cc
    animator.cc
    wnetmodel.cc
    nam_stream.cc
    enetmodel.cc
    testview.cc
    parser.cc
    trafficsource.cc
    lossmodel.cc
    queuehandle.cc
)

add_executable(nam ${nam_SRC})
target_link_libraries(nam ${TCL_LIBRARY} ${TCL_STUB_LIBRARY} ${TK_LIBRARY} ${TK_STUB_LIBRARY} ${OTCL_LIBRARIES} ${TCLCL_LIBRARIES} X11)
install(TARGETS nam RUNTIME DESTINATION ${BIN_INSTALL_DIR})

install(FILES nam.1 DESTINATION ${MAN_INSTALL_DIR}/man1)

#=========================================================================================
### test
enable_testing()
add_test(NAME nam_TEST COMMAND ./validate)
