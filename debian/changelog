nam (1.15-9) unstable; urgency=medium

  * QA upload.

  * Added 1010-const-string-literals.patch to fix incorrect constness of strings.
  * Added multiarch hint, nam-examples 'foreign' and nam 'no'.
  * Removed obsolete lintian override outdated-autotools-helper-file.
  * Added patch metadata.
  * Introduced meaningful patch numbering scheme.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 07 Oct 2024 08:35:16 +0200

nam (1.15-8) unstable; urgency=medium

  * QA upload.

  * Added 1000-gcc-14.patch to fix missing variable type (Closes: #1075302).

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 06 Oct 2024 09:28:22 +0200

nam (1.15-7) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 3.9.8 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Trim trailing whitespace.
  * Transition to automatic debug package (from: nam-dbg).
  * Move source package lintian overrides to debian/source.
  * Bump debhelper from deprecated 8 to 10.
  * Fix field name case in debian/control (Vcs-git ⇒ Vcs-Git).
  * Update Vcs fields in d/control to salsa.
  * Skip autoreconf for now as it do not work.
  * Updated to debhelper compat level 13.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 17 May 2024 14:07:53 +0200

nam (1.15-6) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group <packages@qa.debian.org>. (see: #999656)
  * debian/control: Remove Uploaders field.

 -- Marcos Talau <talau@debian.org>  Tue, 15 Nov 2022 10:14:00 -0300

nam (1.15-5.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix Maintainer email address (Closes: #979052)

 -- Baptiste Beauplat <lyknode@cilg.org>  Tue, 02 Feb 2021 19:55:11 +0100

nam (1.15-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 01:09:06 +0100

nam (1.15-5) unstable; urgency=medium

  * Team upload.
  * Use tracker.d.o address (Closes: #899778)

 -- Aron Xu <aron@debian.org>  Sun, 05 Aug 2018 20:48:39 +0800

nam (1.15-4) unstable; urgency=medium

  [ YunQiang Su ]
  * Fix FTBFS with GCC 6: narrowing conversion (Closes: #811737)
  * Change email to @debian.org
  * Bump standard version to 3.9.8

 -- YunQiang Su <syq@debian.org>  Wed, 02 Nov 2016 22:37:23 +0800

nam (1.15-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control:
      - set tcl-dev and tk-dev to '>=8.6'.
  * debian/patches:
      - init_tcltk_with_stub.diff not in use.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sun, 14 Feb 2016 13:48:32 -0200

nam (1.15-3) unstable; urgency=low

  [ Sergei Golovan ]
  * Defined USE_INTERP_RESULT macro to quickfix FTBFS with Tcl/Tk 8.6.
    (Closes: #725015)

  [ YunQiang Su ]
  * Bump standards version to 3.9.5
  * Fix Git URL

 -- YunQiang Su <wzssyqa@gmail.com>  Sun, 9 Feb 2014 23:29:55 +0800

nam (1.15-2) unstable; urgency=low

  * Team upload.
  * Added missing lib to link, fixing FTBFS.
    Thanks to Daniel T. Chen. (Closes: #713694)
  * Remove DMUA, not needed anymore.

 -- Aron Xu <aron@debian.org>  Wed, 24 Apr 2013 17:34:05 -0400

nam (1.15-1) unstable; urgency=low

  * New upstream stable release.
  * Rewrite build system with CMake.
  * New nam-dbg package.

 -- YunQiang Su <wzssyqa@gmail.com>  Thu, 24 Nov 2011 14:14:34 +0800

nam (1.15~RC4-5) unstable; urgency=low

  *rebuild for work with a patch of tclsh
       remove "-exact" require for tcl and tk.
  * debian/control:
  	fix VCS to true web.

 -- YunQiang Su <wzssyqa@gmail.com>  Thu, 30 Jun 2011 19:08:33 +0800

nam (1.15~RC4-4) unstable; urgency=low

  * Patch to restrict version depend on tk (Closes: #620093)
     Whether it work should be test: init_tcltk_with_stub.diff.
  * fix a bug that abort caused by call random: random-abort.diff.
  * debian/control:
      - Enable Maintainer Upload and set the team to
        Debian Network Simulators Team
        	<pkg-netsim-devel@lists.alioth.debian.org>
      - migrate git to alioth
      		(http://anonscm.debian.org/git/pkg-netsim/nam.git).
  * debian/rules:
       - add -ltclstub8.x and -ltkstub8.x to link against stub.

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 03 Jun 2011 01:50:10 +0800

nam (1.15~RC4-3) unstable; urgency=low

  * patch it for move tclcl's header files to /usr/include/tclcl
  * Build failed ( -lz ), patch it.

 -- YunQiang Su <wzssyqa@gmail.com>  Tue, 22 Mar 2011 23:55:58 +0800

nam (1.15~RC4-2) unstable; urgency=low

  * depends on tk-dev and tcl-dev instead of 8.4 one
  * nam suggests nam-examples (Closes: #586169)

 -- YunQiang Su <wzssyqa@gmail.com>  Mon, 28 Feb 2011 01:11:58 +0800

nam (1.15~RC4-1) unstable; urgency=low

  * New upstream version.
  * port to Policy 3.9.1.

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 17 Sep 2010 14:54:54 +0800

nam (1.15~RC3-2) unstable; urgency=low

  * add suggests of nam-examples on nam (Closes: #586169)
  * port to Policy 3.9.0

 -- YunQiang Su <wzssyqa@gmail.com>  Wed, 30 Jun 2010 14:34:38 +0800

nam (1.15~RC3-1) unstable; urgency=low

  * Initial release (Closes: #576259)

 -- YunQiang Su <wzssyqa@gmail.com>  Fri, 02 Apr 2010 14:34:38 +0800
